package test.hussein.spring_mvc_transactions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import test.hussein.spring_mvc_transactions.controllers.PurchaseController;


public class PurchaseControllerTest {


	private MockMvc mockMvc;
	
	@InjectMocks
	private PurchaseController purhcaseController;
	
	@Before
	public void setup() {
		
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(purhcaseController).build();
	}
	
	@Test
	public void testController() throws Exception {
		mockMvc.perform(get("/api/v1/hello"))
		.andExpect(status().isOk())
		.andExpect(content().string("hello"));
	}
}
