package test.hussein.spring_mvc_transactions.Exceptions;

@SuppressWarnings("serial")
public class BalanceNotSufficientException extends Exception {

	public BalanceNotSufficientException(String message) {
		super(message);
	}

}
