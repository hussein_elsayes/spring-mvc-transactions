package test.hussein.spring_mvc_transactions.ServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import test.hussein.spring_mvc_transactions.Repositories.OrderRepository;
import test.hussein.spring_mvc_transactions.Services.OrderService;
import test.hussein.spring_mvc_transactions.models.Order;

@Service
public class OrderServiceImpl implements OrderService{

	@Autowired
	private OrderRepository orderRepo;
	
	@Transactional(propagation = Propagation.SUPPORTS)
	public void addOrder(Order order) {
		orderRepo.save(order);
	}

}
