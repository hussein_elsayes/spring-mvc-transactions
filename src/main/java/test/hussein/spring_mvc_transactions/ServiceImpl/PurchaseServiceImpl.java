package test.hussein.spring_mvc_transactions.ServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import test.hussein.spring_mvc_transactions.Exceptions.BalanceNotSufficientException;
import test.hussein.spring_mvc_transactions.Services.CustomerService;
import test.hussein.spring_mvc_transactions.Services.OrderService;
import test.hussein.spring_mvc_transactions.Services.PurchaseService;
import test.hussein.spring_mvc_transactions.models.Customer;
import test.hussein.spring_mvc_transactions.models.Order;
@Service
public class PurchaseServiceImpl implements PurchaseService {

	@Autowired
	private OrderService orderService;
	
	@Autowired
	private CustomerService customerService;
	
	@Transactional(propagation = Propagation.REQUIRED , rollbackFor = BalanceNotSufficientException.class)
	public void completePurchase(Customer customer, Order order) throws BalanceNotSufficientException {
		System.out.println("Executing the purchase ...");
		customerService.addCustomer(customer);
		if(order.getOrderAmount() > customer.getBalance()) {
			throw new BalanceNotSufficientException("Balance Not Sufficient !");
		}
		order.setCustomer(customer);
		orderService.addOrder(order);
	}

}
