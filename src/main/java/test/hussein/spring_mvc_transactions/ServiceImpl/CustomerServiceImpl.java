package test.hussein.spring_mvc_transactions.ServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import test.hussein.spring_mvc_transactions.Repositories.CustomerRepository;
import test.hussein.spring_mvc_transactions.Services.CustomerService;
import test.hussein.spring_mvc_transactions.models.Customer;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepo;
	
	@Transactional(propagation = Propagation.MANDATORY)
	public void addCustomer(Customer customer) {
		customerRepo.save(customer);
	}

}
