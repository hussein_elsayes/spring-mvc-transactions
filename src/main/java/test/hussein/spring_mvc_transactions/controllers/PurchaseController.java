package test.hussein.spring_mvc_transactions.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import test.hussein.spring_mvc_transactions.Exceptions.BalanceNotSufficientException;
import test.hussein.spring_mvc_transactions.Services.PurchaseService;
import test.hussein.spring_mvc_transactions.models.Customer;
import test.hussein.spring_mvc_transactions.models.Order;

@RestController
@RequestMapping("/api/v1")
public class PurchaseController {

	@Autowired
	private PurchaseService purchaseService; 
	
	@GetMapping("/hello")
	public String hello() {
		return "hello";
	}
	
	@PostMapping("/placeOrder")
	public void placeOrder(@RequestBody Order order) throws BalanceNotSufficientException {
		Customer customer = new Customer("Customer 1", 1200);
		System.out.println("customer Id :" + customer.getCustomerId());
		purchaseService.completePurchase(customer, order);
	}
}
