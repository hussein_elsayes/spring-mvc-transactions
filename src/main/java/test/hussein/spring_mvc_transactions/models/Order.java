package test.hussein.spring_mvc_transactions.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.sun.istack.NotNull;

@Entity
@Table(name="Purchase_Order")
public class Order {
	public Order() {}
	public Order(double orderAmount, Customer customer) {
		this.OrderAmount = orderAmount;
		this.customer = customer;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long OrderId;
	@NotNull
	private double OrderAmount;
	@ManyToOne
	@JoinColumn(name="customer_id")
	private Customer customer;
	
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public Long getOrderId() {
		return OrderId;
	}
	public void setOrderId(Long OrderId) {
		this.OrderId = OrderId;
	}
	public double getOrderAmount() {
		return OrderAmount;
	}
	public void setOrderAmount(double OrderAmount) {
		this.OrderAmount = OrderAmount;
	}
	
}
