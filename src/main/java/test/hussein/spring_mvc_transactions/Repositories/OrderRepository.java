package test.hussein.spring_mvc_transactions.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import test.hussein.spring_mvc_transactions.models.Order;
@Repository

public interface OrderRepository extends JpaRepository<Order, Long> {

}
