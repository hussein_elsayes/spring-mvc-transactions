package test.hussein.spring_mvc_transactions.Services;

import test.hussein.spring_mvc_transactions.models.Customer;


public interface CustomerService {
	public void addCustomer(Customer customer);
}
