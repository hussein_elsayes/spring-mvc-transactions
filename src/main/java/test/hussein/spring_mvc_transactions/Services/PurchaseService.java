package test.hussein.spring_mvc_transactions.Services;

import test.hussein.spring_mvc_transactions.Exceptions.BalanceNotSufficientException;
import test.hussein.spring_mvc_transactions.models.Customer;
import test.hussein.spring_mvc_transactions.models.Order;

public interface PurchaseService {
	public void completePurchase(Customer customer, Order order) throws BalanceNotSufficientException;
}
