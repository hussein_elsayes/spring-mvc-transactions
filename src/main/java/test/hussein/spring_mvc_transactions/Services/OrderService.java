package test.hussein.spring_mvc_transactions.Services;

import test.hussein.spring_mvc_transactions.models.Order;

public interface OrderService {

	public void addOrder(Order order);
}
