package test.hussein.spring_mvc_transactions.aspect;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class TransactionLoggingAspect {

	//Logger logger = Logger.getLogger(getClass());
	
	@Pointcut("execution(* completePurchase(..))")
	public void ServicesLogging() {}
	
	@Before("ServicesLogging()")
	public void Log(JoinPoint joinPoint) {
		//logger.log(null, "=======> Starting executing  " + joinPoint.toShortString());
		System.out.println("=======> Starting executing  " + joinPoint.toShortString());
	}
	
	@Around("ServicesLogging()")
	public void BeforeAndAfter(ProceedingJoinPoint proceedingJoinPoint) {
		System.out.println("");
		
		long startTime = System.currentTimeMillis();
		
		try {
			Object result = proceedingJoinPoint.proceed();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			System.out.println("=======>  Error Happened !! of type : " + e.getClass().getName());
		}
		
		long endTime = System.currentTimeMillis();
		//logger.log(null , "Time elapsed .... " + (endTime - startTime) + " milliseconds ");

		System.out.println("=======>  Time elapsed .... " + (endTime - startTime) + " milliseconds ");
		
	}
	
}
